// Iteración 5

const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
function averageWord(param) {
  let sum=0;
  for (item of param){
      if (typeof(item)=="number"){
            sum+=item;
      }else if (typeof(item)=="string"){
            sum+=item.length;
      }
  }
  return sum;
}

console.log(averageWord(mixedElements));
// Iteración 7

const nameFinder = [
    'Peter',
    'Steve',
    'Tony',
    'Natasha',
    'Clint',
    'Logan',
    'Xabier',
    'Bruce',
    'Peggy',
    'Jessica',
    'Marc'
  ];

  function finderName(param, item) {
    if (param.includes(item)){
        return 'true ' + param.indexOf(item);
    }else{
        return 'false';
    }
  }

  console.log(finderName(nameFinder,'Tony'));

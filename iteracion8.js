// Iteración 8

const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
  ];
  function repeatCounter(param) {
    var repeat = {};

    param.forEach(function(element){
    repeat[element] = (repeat[element] || 0) + 1;
    }   );

    return repeat;
  }

  console.log(repeatCounter(counterWords));
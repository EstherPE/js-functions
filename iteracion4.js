// Iteración 4

const numbers2 = [12, 21, 38, 5, 45, 37, 6];

function sumAll(param) {
  let sum=0;
  for (item of param){
      sum+=item;
  }
  return sum;
}

function average(param) {
  return sumAll(param)/(param.length)
}

console.log(average(numbers2));
